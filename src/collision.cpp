#include "physics/collision.h"

namespace physics {
	namespace algo {
		CollisionPoints find_sphere_sphere_collision(const SphereCollider *sphere_1, const Transform *transform_sphere_1, const SphereCollider *sphere_2, const Transform *transform_sphere_2) {
			const float sum_radius = sphere_1->radius + sphere_2->radius;
			math::vec3 direction = transform_sphere_1->position - transform_sphere_2->position;
			const float distance = direction.norm();
			CollisionPoints points = {.has_collision=distance <= sum_radius};

			if( ! points.has_collision )
				return points;

			direction.normalise();

			// Now need to process the contact points.

			return points;
		}

		CollisionPoints find_sphere_plane_collision(const SphereCollider *sphere, const Transform *transform_sphere, const PlaneCollider *plane, const Transform *transform_plane) {
			return {};
		}

		CollisionPoints find_plane_sphere_collision(const PlaneCollider *plane, const Transform *transform_plane, const SphereCollider *sphere, const Transform *transform_sphere) {
			CollisionPoints points = find_sphere_plane_collision(sphere, transform_sphere, plane, transform_plane);

			math::vec3 tmp = points.A;
			points.A = points.B;
			points.B = tmp;

			points.normal = -points.normal;

			return points;
		}
	} // namespace algo
} // namespace physics
