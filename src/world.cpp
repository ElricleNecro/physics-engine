#include "physics/world.h"

#include <algorithm>

namespace physics {
	void CollisionWorld::erase(CollisionObject *object) {
		if( ! object )
			return;

		auto itr = std::find(this->objects.begin(), this->objects.end(), object);
		if( itr == this->objects.end() )
			return;

		this->objects.erase(itr);
	}

	void CollisionWorld::erase(Solver *solver) {
		if( ! solver )
			return;

		auto itr = std::find(this->solvers.begin(), this->solvers.end(), solver);
		if( itr == this->solvers.end() )
			return;

		this->solvers.erase(itr);
	}

	void CollisionWorld::call_solvers(std::vector<Collision> &collisions, const float dt) {
		for(Solver *solver: this->solvers) {
			solver->solve(collisions, dt);
		}
	}

	void CollisionWorld::call_callbacks(std::vector<Collision> &collisions, const float dt) {
		for(Collision &collision: collisions) {
			if(collision.a->on_collision)
				collision.a->on_collision(collision, dt);

			if(collision.b->on_collision)
				collision.b->on_collision(collision, dt);
		}
	}

	void CollisionWorld::solve_collisions(const float dt) {
		std::vector<Collision> collisions;
		std::vector<Collision> triggers;

		for(CollisionObject *a: this->objects) {
			for(CollisionObject *b: this->objects) {
				if( a == b )
					break;	// Keep only unique pair, plus allow to optimise a little bit better both loops.

				if( ! a->collider || ! b->collider )
					continue;	// One of the objects don't support collisions, continuing.

				CollisionPoints points = a->collider->collision(a->transform, b->collider, b->transform);
				if( points.has_collision ) {
					bool trigger = a->is_trigger || b->is_trigger;

					if( trigger )
						triggers.emplace_back(a, b, points);
					else
						collisions.emplace_back(a, b, points);
				}
			}
		}

		this->call_solvers(collisions, dt);

		this->call_callbacks(collisions, dt);
		this->call_callbacks(triggers, dt);
	}

	void DynamicsWorld::apply_gravity(void) {
		for(CollisionObject *obj: this->objects) {
			if( ! obj->is_dynamic )
				continue;

			RigidBody *rigid_body = static_cast<RigidBody*>(obj);

			// rigid_body->apply_force(rigid_body->gravity);
			rigid_body->acceleration = rigid_body->gravity;
		}
	}

	void DynamicsWorld::move_objects(const float dt) {
		for(CollisionObject *obj: this->objects) {
			if( ! obj->is_dynamic )
				continue;

			RigidBody *rigid_body = static_cast<RigidBody*>(obj);

			rigid_body->velocity += rigid_body->acceleration * dt;
			rigid_body->position += rigid_body->velocity * dt;
		}
	}

	void DynamicsWorld::solve(const float dt) {
		this->apply_gravity();
		this->solve_collisions(dt);
		this->move_objects(dt);
	}
} // namespace physics
