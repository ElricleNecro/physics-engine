workspace "Physics"
	architecture "x86_64"
	cppdialect "C++20"

	configurations { "Debug", "Release" }
		-- includedirs {
		-- }
		warnings "Extra"
		flags "MultiProcessorCompile"

project "physics"
	filename "physics"
	kind "SharedLib"
	language "C++"
	cppdialect "C++20"

	location "build/"
	targetdir "build/%{cfg.buildcfg}/lib"

	systemversion "latest"
	-- staticruntime "Off"

	files { "src/**.cpp" }

	includedirs {
		"include",
	}

	-- links {
	-- }

	-- libdirs {
	-- }

	filter "configurations:Debug"
		defines { "DEBUG", "PHYSICS_DEBUG", "PHYSICS_PROFILE" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"
		flags { "LinkTimeOptimization", "FatalWarnings" }
