#pragma once

#include <cmath>

#ifndef PHYSICS_CONSIDERED_ZERO
#define PHYSICS_CONSIDERED_ZERO 1e-4
#endif

namespace physics {
	namespace math {
		struct vec3 {
			float x, y, z;

			inline vec3 operator*(const float right) const {
				return {this->x * right, this->y * right, this->z * right};
			}

			inline void operator*=(const float right) {
				this->x *= right;
				this->y *= right;
				this->z *= right;
			}

			inline vec3 operator+(const float right) const {
				return {this->x + right, this->y + right, this->z + right};
			}

			inline void operator+=(const float right) {
				this->x += right;
				this->y += right;
				this->z += right;
			}

			inline vec3 operator+(const vec3 &right) const {
				return {this->x + right.x, this->y + right.y, this->z + right.z};
			}

			inline void operator+=(const vec3 &right) {
				this->x += right.x;
				this->y += right.y;
				this->z += right.z;
			}

			inline vec3 operator-(const float right) const {
				return {this->x - right, this->y - right, this->z - right};
			}

			inline void operator-=(const float right) {
				this->x -= right;
				this->y -= right;
				this->z -= right;
			}

			inline vec3 operator-(void) const {
				return {-this->x, -this->y, -this->z};
			}

			inline vec3 operator-(const vec3 &right) const {
				return {this->x - right.x, this->y - right.y, this->z - right.z};
			}

			inline void operator-=(const vec3 &right) {
				this->x -= right.x;
				this->y -= right.y;
				this->z -= right.z;
			}

			inline float norm2(void) const {
				return this->x * this->x + this->y * this->y + this->z * this->z;
			}

			inline float norm(void) const {
				return std::sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
			}

			inline void normalise(void) {
				// Avoid dividing by zero:
				if( std::fabs(this->norm()) < PHYSICS_CONSIDERED_ZERO )
					return;

				this->x /= this->norm();
				this->y /= this->norm();
				this->z /= this->norm();
			}
		};
	} // namespace math
} // namespace physics
