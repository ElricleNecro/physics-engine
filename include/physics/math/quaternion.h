#pragma once

namespace physics {
	namespace math {
		struct quaternion {
			float mat[4*4] = {0};
		};
	}
}
