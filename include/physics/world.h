#pragma once

#include <vector>

#include "physics/object.h"

namespace physics {
	class Solver {
	public:
		virtual void solve(const std::vector<Collision> &collisions, const float dt) = 0;
	};

	class CollisionWorld {
	public:
		CollisionWorld(void) = default;
		
		inline void push_back(CollisionObject *object) {
			this->objects.push_back(object);
		}
		inline void push_back(Solver *solver) {
			this->solvers.push_back(solver);
		}

		void erase(CollisionObject *object);
		void erase(Solver *solver);

		void solve_collisions(const float dt);

	protected:
		std::vector<CollisionObject*> objects;
		std::vector<Solver*> solvers;

		void call_solvers(std::vector<Collision> &collisions, const float dt);
		void call_callbacks(std::vector<Collision> &collisions, const float dt);
	};

	class DynamicsWorld : public CollisionWorld {
	public:
		DynamicsWorld(void) = default;

		inline void push_back(RigidBody *object) {
			if( object->use_world_gravity )
				object->gravity = this->gravity;

			this->objects.push_back(object);
		}

		void solve(const float dt);

	private:
		math::vec3 gravity{0.f, -9.81f, 0.f};

		void apply_gravity(void);
		void move_objects(const float dt);
	};
} // namespace physics
