#pragma once

#include <functional>

#include "physics/math/vec3.h"
#include "physics/collision.h"

namespace physics {
	struct Collision;
	struct CollisionObject {
		Collider *collider;
		Transform *transform;

		bool is_dynamic;
		bool is_trigger;

		std::function<void(Collision&, const float)> on_collision;
	};

	struct Collision {
		CollisionObject *a, *b;
		CollisionPoints point;
	};

	struct RigidBody : public CollisionObject {
		math::vec3 position;
		math::vec3 velocity;
		math::vec3 acceleration;

		math::vec3 gravity;
		bool use_world_gravity;

		float static_friction;
		float dynamic_friction;
		float restitution;
	};
} // namespace physics
