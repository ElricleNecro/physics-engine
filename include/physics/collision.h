#pragma once

#include "physics/math/vec3.h"
#include "physics/math/quaternion.h"

namespace physics {
	struct CollisionPoints {
		math::vec3 A;			// Furthest point of A into B;
		math::vec3 B;			// Furthest point of B into A;
		math::vec3 normal;		// B - A normalised;

		float depth;		// Length of B - A;
		bool has_collision;
	};

	struct Transform {
		math::vec3 position;
		math::vec3 scale;
		math::quaternion rotation;
	};

	struct PlaneCollider;
	struct SphereCollider;
	struct Collider {
		virtual CollisionPoints collision(
			const Transform *transform,
			const Collider *collider,
			const Transform *collider_transform
		) const = 0;
		virtual CollisionPoints collision(
			const Transform *transform,
			const SphereCollider *collider,
			const Transform *collider_transform
		) const = 0;
		virtual CollisionPoints collision(
			const Transform *transform,
			const PlaneCollider *collider,
			const Transform *collider_transform
		) const = 0;
	};

	namespace algo {
		CollisionPoints find_sphere_sphere_collision(const SphereCollider *sphere_1, const Transform *transform_sphere_1, const SphereCollider *sphere_2, const Transform *transform_sphere_2);
		CollisionPoints find_sphere_plane_collision(const SphereCollider *sphere, const Transform *transform_sphere, const PlaneCollider *plane, const Transform *transform_plane);
		CollisionPoints find_plane_sphere_collision(const PlaneCollider *plane, const Transform *transform_plane, const SphereCollider *sphere, const Transform *transform_sphere);
	} // namespace algo

	struct SphereCollider : Collider {
		math::vec3 center;
		float radius;

		inline virtual CollisionPoints collision(
			const Transform *transform,
			const Collider *collider,
			const Transform *collider_transform
		) const override {
			// Using double dispatch:
			return collider->collision(collider_transform, this, transform);
		}

		virtual CollisionPoints collision(
			const Transform *transform,
			const SphereCollider *collider,
			const Transform *collider_transform
		) const override {
			return algo::find_sphere_sphere_collision(this, transform, collider, collider_transform);
		}

		virtual CollisionPoints collision(
			const Transform *transform,
			const PlaneCollider *collider,
			const Transform *collider_transform
		) const override {
			return algo::find_sphere_plane_collision(this, transform, collider, collider_transform);
		}
	};

	struct PlaneCollider : Collider {
		math::vec3 plane;
		float distance;

		inline virtual CollisionPoints collision(
			const Transform *transform,
			const Collider *collider,
			const Transform *collider_transform
		) const override {
			return collider->collision(collider_transform, this, transform);
		}

		virtual CollisionPoints collision(
			const Transform *transform,
			const SphereCollider *collider,
			const Transform *collider_transform
		) const override {
			return algo::find_plane_sphere_collision(this, transform, collider, collider_transform);
		}

		inline virtual CollisionPoints collision(
			[[maybe_unused]]const Transform *transform,
			[[maybe_unused]]const PlaneCollider *collider,
			[[maybe_unused]]const Transform *collider_transform
		) const override {
			return {};
		}
	};
}
